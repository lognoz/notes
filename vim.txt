                            Vim (text editor)


  I. Viewport Movement

   zz    Ajusting cursor in the middle of screen
   H     High of the screen
   M     Middle of the screen
   L     Low of the screen
   3L    3 before low of screen. This fonctionnality works with H, M and L
   {     Jump to next paragraph
   }     Jump to previous paragraph
   .'    Move cursor to previously modified line
   %     Place cursor on {}[]() and type "%"


  II. Words Movement

   w     Start of a word
   e     End of a word
   E     End of a word (words can contain punctuation)
   b     Backwards to the start of a word
   B     Backwards to the start of a word (words can contain punctuation)
   ge    Backwards to the end of a word
   0     Start of the line
   $     End of the line, (2$ to the end of the next line)
   ^     Moves to the first non-whitespace character in the line
   fx    Jump to next occurrence of character x
   Fx    Jump to previous occurence of character x
   tx    Work like fx command, except it stops one character before x
   ;     Repeat previous f, t, F or T movement
   ,     Repeat previous f, t, F or T movement, backwards


  III. Change

   r     Replace a single character
   R     Replace multiple character
   J     Join the current line with the next one
   .     Repeat last change
   s     Start insert mode and remove a character
   S     Change the current line
   C     Change to the end of the line

  IV. Insert

   i     Insert before the cursor
   I     Insert at the beginning of the line
   a     Insert (append) after the cursor
   A     Insert (append) at the end of the line
   o     Insert a new line below the current line
   O     Insert a new line above the current line


  V. Visual

   v       Select a part
   V       Select line by line
   ctl+v   Flexible select
   gv      Reselect the last visual selection
   o       Go to other end of highlighted text in visual selection
   vrx     Replace each character in selection with x
   gv      Reselect last selection


  VI. Change, Delete and Visual combinaisons

   You need to use 'd', 'c' or 'v' before these combinaisons.

   iw   Affect the word
   aw   Affect a word
   ip   Affect paragraph
   it   Affect in tag
   t{   Affect all the centent before {, and keep {
   f{   Affect the content before and {
   it   Affect all content in a tag: <div></div>
   i(   Affect content in ()


  VII. Delete

   D       Cut the end of the line
   dd      Delete line
   x       Delete letter


  IX. Commands

    :'<,'>sort       Sort a selection
    :'<,'>sort!      Usort a selection
    :'<,'>normal .   For each line in the visual selection, execute the
                     Normal mode . command


  X. Math

   Open math tab        i, CTRL-R, =
   Paste in math tab    CTRL-R, "


  XI. Fuzzy file

   Search in all files    CTRL-P
   Search in open files   CTRL-P, CTRL-F


  XII. Autocomplete

   Complete a word   CTRL-X, CTRL-P
   Complete a line   CTRL-X, CTRL-L


  XIII. Surround

   Surround word by double quote           yss"
   Change double quote by singles quote    cs"'
   Replace single quote by tag             cs'<p>
   Remove delimiters                       ds"


  XIV. Encryption

   Put this line in your vimrc: set cm=blowfish2
     :X   Encrypt current file

   You can also use rot13 encryption:
     g??  Encrypt current line
     g?   Encrypt a selection


  XV. Folding

   Open folding         zo
   Open all folding     zi
   Close folding        zc


  XVI. Fonctionality

   Count words in document   g<C-G>


  XVII. Increment and decrement

   Increment   CTRL+A
   Decrement   CTRL+X


  XVIII. Undoing

    u        Undo
    U        Undo line
    CTRL+R   Redo


  XIX. Search

   /x       Search forward for pattern
   ?x       Search backward
   /<Up>    Search history
   n        Next match
   N        Reverse direction and repeat the search
   *        Search for word currently under cursor
   #        Search backward for word currently under cursor


  XX. Change case

   ~       Change character or a selection
   U       Uppercase a selection
   u       Lowercase a selection
   gUiw    Change current word to uppercase
   g~iw    Toggle case of the current word
   x~      Toggle case of the next x characters


  XXI. Marks

   mx       Define mark as x
   'x       Go to the mark line
   :marks   List all the marks


  XXII. Dealing with text files

   Format a selection    gq
   Define text width     :set textwidth=70

   Section moving        Add page break character with CTRL+L in insert
                         mode. The ]] and [[ commands perform the section
                         mouvement.

   Align a selection     :right <margin>
                         :left <margin>
                         :center <margin>


  XXIII. Regular expressions

   x       The literal character x
   ^       Start of line
   $       End of line
   .       A single character
   \<x     A word that start with x
   x\>     A word that end with x


  XXIV. Yank and put

   Y      Yank the current line
   yy     Yank the current line (same as Y)
   yiw    Yank the current word
   y'a    Yank to marker a
   p      Put after cursor
   P      Put before cursor
   gp     Put and left the cursor at the end of the new text
   2p     Put back the second text stored in the register


  XXV. Special registers

   To put text contain on another register, you can use the double quote
   ("). For example, if I want to put the last yanked text, I can write "0p

   0     The last yanked text
   -     The last delete
   .     The last inserted text
   %     The name of the current file
   #     The name of the alternate file
   /     The last search string
   :     The last ":" command
   _     The black hole
   =     An expression
   *     The text selected with the mouse


  XXVI. Black hole register

   The black hole register is useful when you want to delete text without
   having to yank it. For example, dd deletes a line and store it in the
   position 1 of p fonctionality. The command "_dd deletes a line and
   leaves 1 alone.


  XXVII. Expression register

   The expression register is designed so that you can enter expressions
   into text. For example, "=38*56<enter>p give you 2128. An expression
   can contain all the usual arthmetic operators (*, +, -, /).


  XXVIII. Bibliography

   https://devhints.io/vimscript
   https://www.cse.iitb.ac.in/~avadhut/sysadgiri/vim/vimtips.html
   https://github.com/LeCoupa/awesome-cheatsheets/blob/master/tools/vim.txt
